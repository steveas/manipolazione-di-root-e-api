import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EdituserComponent } from './edituser/edituser.component';
import { InfouserComponent } from './infouser/infouser.component';
import { NewuserComponent } from './newuser/newuser.component';
import { PostuserComponent } from './postuser/postuser.component';
import { TableuserComponent } from './tableuser/tableuser.component';

const routes: Routes = [
  {path: 'tableuser', component: TableuserComponent},
  {path: 'newuser', component: NewuserComponent},
  {path: 'infouser/:url/info', component: InfouserComponent},
  {path: 'edituser/:id', component: EdituserComponent},
  {path: 'postuser/:id', component: PostuserComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
