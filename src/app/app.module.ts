import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './header/header.component';
import { NewuserComponent } from './newuser/newuser.component';
import { TableuserComponent } from './tableuser/tableuser.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { InfouserComponent } from './infouser/infouser.component';
import { EdituserComponent } from './edituser/edituser.component';
import { PostuserComponent } from './postuser/postuser.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NewuserComponent,
    TableuserComponent,
    InfouserComponent,
    EdituserComponent,
    PostuserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
