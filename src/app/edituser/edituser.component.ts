import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Iuser } from '../interfaces/iuser';
import { UserserviceService } from '../services/userservice.service';

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.css']
})
export class EdituserComponent implements OnInit {

  editUser!:Iuser;

  constructor(
      private route: ActivatedRoute, 
      private userService: UserserviceService, 
      private router: Router
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      let obj = this.userService.getUser(params['id']);
      if(obj) {
        this.editUser = obj;
      }  else {
        this.router.navigate(['tableuser'])
      }
    });
  }

  saveUser() {
    this.userService.updateUser(this.editUser);
    this.router.navigate(['tableuser'])
  }

}
