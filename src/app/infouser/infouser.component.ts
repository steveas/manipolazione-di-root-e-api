import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../classes/user';
import {Iuser} from '../interfaces/iuser';
import { UserserviceService } from '../services/userservice.service';

@Component({
  selector: 'app-infouser',
  templateUrl: './infouser.component.html',
  styleUrls: ['./infouser.component.css']
})
export class InfouserComponent implements OnInit {

  infoUser: any= [];

  constructor(
      private route: ActivatedRoute, 
      private userService: UserserviceService, 
      private router: Router
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(response => {
      this.userService.getUserByURL(response['url']).subscribe(response => {
        this.infoUser = response;
        
      })
    });
  }

  updateUser(item: Iuser) {
    
    this.router.navigate(['edituser', item.id]);
  }

  seePost(item: Iuser){

    this.router.navigate(['postuser', item.id]);


  }
}