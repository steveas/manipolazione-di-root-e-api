export interface Iuser {
    id?: number,
    name: string,
    username: string,
    email: string,
    phone: string,
    website: string,
    url?: string

}
