import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Iuser } from '../interfaces/iuser';
import { User } from '../classes/user';
import { UserserviceService } from '../services/userservice.service';

@Component({
  selector: 'app-newuser',
  templateUrl: './newuser.component.html',
  styleUrls: ['./newuser.component.css']
})
export class NewuserComponent implements OnInit {

  newUser: Iuser = new User();

  constructor(private userService: UserserviceService, private router: Router) { }

  ngOnInit(): void {
  }

  addUser() {
    this.userService.addUser(this.newUser);
    this.router.navigate(['tableuser']);
  }

}
