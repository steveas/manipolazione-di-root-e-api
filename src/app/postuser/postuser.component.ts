import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Iuser } from '../interfaces/iuser';
import { UserserviceService } from '../services/userservice.service';
import { Ipost } from '../interfaces/ipost';
import { PostserviceService } from '../services/postservice.service';
import { User } from '../classes/user';

@Component({
  selector: 'app-postuser',
  templateUrl: './postuser.component.html',
  styleUrls: ['./postuser.component.css']
})
export class PostuserComponent implements OnInit {

  newPost: Ipost[]=[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private postService: PostserviceService,
    private userService: UserserviceService
  ) { }

  ngOnInit(): void {


    this.route.params.subscribe(params => 
      {
        this.newPost= this.postService.getAllPost();
        this.newPost= this.newPost.filter(element => element.userId == params['id'])
        console.log(this.newPost)

      })
  }

  seeCommento(){
    
  }

}
