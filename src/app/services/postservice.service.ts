import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Iuser } from '../interfaces/iuser';
import { environment } from '../../environments/environment';
import {User} from '../classes/user';
import { Ipost } from '../interfaces/ipost';

@Injectable({
  providedIn: 'root'
})
export class PostserviceService {

  Postlist: Ipost[]= [];

  constructor(
    private http: HttpClient
    ) {
      this.http.get<Ipost[]>(environment.postAPI).subscribe(response => this.Postlist = response);
      
     }

    getAllPost() {
      return this.Postlist;
    }


  
}
