import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Iuser } from '../interfaces/iuser';
import { environment } from '../../environments/environment';
import {User} from '../classes/user';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  users: Iuser[]=[];

  constructor(private http: HttpClient) {
    this.http.get<Iuser[]>(environment.userAPI).subscribe(response => this.users = response);
   }

  

  getUserByURL(url: string) {
    return this.http.get<Iuser>(url);
  }

  getAllUser() {
    return this.users;
  }

  getUser(id: number) {    
    //this.http.get(this.urlAPI+id).subscribe(response => console.log(response))
    let obj = this.users.find(element => element.id == id);
    return obj;
  }

  addUser(user: Iuser) {
    this.http.post<Iuser>(environment.userAPI, user).subscribe(response => {
      this.users.push(response);
    });
  }

  deleteUser(user: Iuser) {
    this.http.delete(environment.userAPI+user.id).subscribe(response => {
      this.users = this.users.filter(elements => elements.id !== user.id);
    })
  }

  updateUser(user: Iuser) {
    this.http.put<Iuser>(environment.userAPI+user.id, user).subscribe(response => {
      let obj = this.users.find(element => element.id === user.id);
      if(obj) {
        let index = this.users.indexOf(obj);
        this.users.splice(index, 1, response);
      }
    })
  }






}
