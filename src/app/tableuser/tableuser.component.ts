import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Iuser} from '../interfaces/iuser';
import {UserserviceService} from '../services/userservice.service';
import { EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';
import { identifierModuleUrl } from '@angular/compiler';

@Component({
  selector: 'app-tableuser',
  templateUrl: './tableuser.component.html',
  styleUrls: ['./tableuser.component.css']
})
export class TableuserComponent implements OnInit {

  listuser: Iuser[]=[];

  constructor(private userService: UserserviceService, private router: Router) { }

  ngOnInit(): void {
    setTimeout(() => {
      this.listuser = this.userService.getAllUser();
    }, 500)
  }

  removeUser(item: Iuser) {
    this.userService.deleteUser(item);
    this.listuser = this.listuser.filter(elements => elements.id !== item.id);
  }

  infoUser(item: Iuser) {

    //fare la funzione che sposta la route e porta con se gli elementi in lettura
    item.url= environment.userAPI + item.id;
    console.log(item);
    console.log(item.url);
    this.router.navigate(['infouser', item.url, 'info']);
    
  }


}
